import React, { useEffect, useState } from 'react';
import './App.css';
import Tours from './components/Tours';
import Loading from './components/Loading';
const url = 'https://course-api.com/react-tours-project'

function App() {
  const [loading, setLoading] = useState(true);
  const [tours, setTours] = useState([]);

  const removeTour = (id) => {
    // const newTours = tours.filter((tour) => tour.id !== id);
    // setTours(newTours);
    const newTours = [];
    for (let i = 0; i < tours.length; i++){
      if(tours[i].id !== id){
        newTours.push(tours[i]);
      }
      setTours(newTours);
    }
  }

  const fetchTours = async() => {
    setLoading(true);
    try {
      const data = await fetch(url);
      const tours = await data.json();
      console.log(tours);
      setLoading(false);
      setTours(tours);

    } catch (error) {
      setLoading(false);
      console.log(error);  
    }
  }

  useEffect(() => {
    fetchTours();
  }, []);

  if(tours.length === 0){
    return(
      <div className="App">
        <div className="title">
          <h2>No Tours Left</h2>
          <button className="refresh" onClick={() => fetchTours()}>Refresh</button>
        </div>
      </div>
    )
  }

  if(loading){
    return(
      <div className="App">
        <Loading/>
      </div>
    );
  }
  return(
    <div className="App">
      <Tours tours={tours} removeTour={removeTour}/>
    </div>
  ); 
}

export default App;
