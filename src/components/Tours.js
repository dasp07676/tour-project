import React from 'react'
import Tour from './Tour'

const Tours = ({tours, removeTour}) => {
    return(
        <div className="tour">
            <div className='title'>
                <h1>Our Tours</h1>
            </div>
            <div>
                {tours.map((tour) => {
                    return <Tour key={tour.id} {...tour} removeTour={removeTour}></Tour>
                })}
            </div>
        </div>
    )
}

export default Tours;